package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LandingPage{
	//constructor of the LandingPage bringing the driver and concatenate it with find element

		public LandingPage (AndroidDriver<AndroidElement> driver) {
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		}
	

		@AndroidFindBy(xpath="//android.widget.TextView[@text='Series']")
		private WebElement series;
		public WebElement getSeries() {
			return series;
		}	
		
		@AndroidFindBy(xpath="//android.widget.TextView[@index='0']")
		private WebElement mousalsalat;
		public WebElement getMousalsalat() {
			return mousalsalat;
		}	
		
		@AndroidFindBy(xpath="//android.view.ViewGroup[@index='2']")
		private WebElement profile;
		public WebElement getProfile() {
			return profile;
		}	

				@AndroidFindBy(xpath="//android.widget.TextView[@text='Language - EN']")
		private WebElement languageSelection;
		public WebElement getLanguageSelection() {
			return languageSelection;
		}	
				
}
