package shashaAppium.shashaFramework;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseWeb {

	 public static void main(String[] args) {
//		 WebDriver driver = new ChromeDriver();
		 //System.setProperty("webdriver.chrome.driver","/path/to/chromedriver");
		 ChromeDriver driver = new ChromeDriver();
		 System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\Desktop\\chromedriver_win32 (2)");
			

	        driver.get("https://google.com");
	        
	        driver.getTitle(); // => "Google"

	        //driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));
	        
	        WebElement searchBox = driver.findElement(By.name("q"));
	        WebElement searchButton = driver.findElement(By.name("btnK"));
	        
	        searchBox.sendKeys("Selenium");
	        searchButton.click();
	        
	        searchBox = driver.findElement(By.name("q"));
	        searchBox.getAttribute("value"); // => "Selenium"
	        
	        driver.quit();
	    }
	
}
