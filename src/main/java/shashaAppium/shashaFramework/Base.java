package shashaAppium.shashaFramework;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class Base {
	//initialize/declare our driver at global level inside Base
	public static AndroidDriver<AndroidElement>driver;
	
	
	//function to start emulator device	
	//**************************here
		public static void startEmulator() throws IOException, InterruptedException {
			Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\src\\main\\java\\resources\\startEmulator.bat");
			//Thread.sleep(30000);
		}
	
	//function to start appium server
	//**************************here
	public static AppiumDriverLocalService service;
	public AppiumDriverLocalService StartServer() {
		boolean flag = checkIfServerIsRunning(4723); 
		if(!flag) {
			service=AppiumDriverLocalService.buildDefaultService();
			service.start();	
		}
			return service;
	}
	
	//function to check if the server is already running on a specific port
//**************************here
	public static boolean checkIfServerIsRunning(int port) {
		boolean isServerRunning = false;
		ServerSocket serverSocket;
		try {
			serverSocket=new ServerSocket(port);
			serverSocket.close();
		}catch (IOException e) {
			//if control comes here it means the port is in use
			isServerRunning=true;
		}finally {
			serverSocket=null;	
		}
		return isServerRunning;
	}
	
	

	public static AndroidDriver<AndroidElement>capabilities(String appName) throws IOException, InterruptedException {
		
		//to load the file global.properties and get the props from there
		//System.getProperty("user.dir"): same like C:\Users\User\eclipse-workspace\shashaFramework
		//but not hard coded
		
		FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"\\src\\main\\java\\shashaAppium\\shashaFramework\\global.properties");
		Properties prop=new Properties();
		prop.load(fis);
			
		
		
		File appDir = new File("src");
		File app = new File (appDir,(String) prop.get(appName));
		
		//File f = new File("src");
		//File fs = new File (f,"ShashaApp.apk");
		
		DesiredCapabilities capabilities= new DesiredCapabilities();		
		
		String device = prop.getProperty("device");
		
		//System.out.println("tttttttttthussss isss device "+device);

		//we can use the deviceName entered from maven command at system runtime
		//String device=System.getProperty("deviceName");

		//check if the device name that we got from global.properties has the word emulator to run the bat file
		
		//********************************here
		if(device.contains("emulator")) {
			startEmulator();
			Thread.sleep(20000);
		}
	
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,device);
		
		capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,2000);
		capabilities.setCapability(MobileCapabilityType.APP,app.getAbsolutePath());
		driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
		//driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
		driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		
	//	Thread.sleep(20000);
		return driver;
		
	}
	

	//implement a function or method that take screenshot
		
		public static void getScreenshot(String failedName) throws Exception{
			File screenshotFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshotFile,new File(System.getProperty("user.dir")+"\\src\\main\\java\\resources\\"+failedName+".png"));
			
		}
	
}
