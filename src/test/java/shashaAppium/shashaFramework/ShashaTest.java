package shashaAppium.shashaFramework;


import java.io.IOException;

import org.testng.Assert;
//import org.testng.asserts.SoftAssert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.LandingPage;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

//@Listeners(resources.Listeners.class)


public class ShashaTest extends Base{
	
	@Test
	public void ChangeLanguage() throws Exception{
		//**************************here
	service=StartServer();
		AndroidDriver<AndroidElement>driver=capabilities("ShashaApp");
		Thread.sleep(5000);
	
		LandingPage p =new LandingPage(driver);
		//p.getSeries().click();
		
		p.getProfile().click();
		//p.getMousalsalat().click();
		
		Thread.sleep(15000);
		Utilities u =new Utilities(driver);
		u.scrollToText("Language - EN");

		p.getLanguageSelection().click();
		
		//Thread.sleep(30000);
//		System.out.println(p.getMousalsalat().getText());
		
		
//		String seriesButton = p.getMousalsalat().getText();
//		String expectedSeriesButton = "مسلسلات";
//		Assert.assertEquals(seriesButton, expectedSeriesButton,"the translation didn't happen");
		
		
//		String expectedTitle = "Free QA Automation Tools For Everyone";
//        String originalTitle = driver.getPlatformName();
//        Assert.assertEquals(originalTitle, expectedTitle,"the platform is incorrect");
      
		//*****************here	
		service.stop();
}	
	//create a before test method to kill all the processes that are already running
		//to make sure that the appium server is closed before running any test
		// we need to execute from java the command: taskkill /F /IM node.exe
		//to execute from java, use the Runtime method
		//we can pass this command directly to be executed instead of using a bat file bcz it is only 1 command
		
	//**************************here
		@BeforeTest
		public void KillAllNodes() throws IOException, InterruptedException {
			Runtime.getRuntime().exec("taskkill /F /IM node.exe");
			Thread.sleep(5000);
		}
		
}
